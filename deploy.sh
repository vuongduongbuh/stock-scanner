#!/bin/bash

USER='root'
HOST='suc.headbits.co'
DEST='/var/www/suc.headbits.co'


mkdir dist

au build --env prod
mv scripts dist/
cp index.html dist/index.html

rsync -az --force --delete --progress --prune-empty-dirs -e "ssh -p22" ./dist/* $USER@$HOST:$DEST

rm -r dist