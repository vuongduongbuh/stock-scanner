import { HttpService } from '../services/httpService';
import { json } from 'aurelia-fetch-client';

export class ProfileService {
    constructor() {
        this.httpClient = new HttpService().getClient();
    }

    get() {
        return this.httpClient
            .fetch('/v1/profile')
            .then(response => response.json())
            .then(data => {
                return data;
            });
    }

    patch(changeObj) {
        return this.httpClient
            .fetch('/v1/profile', {
                method: 'PATCH',
                body: json(changeObj)
            })
            .then(response => response.json())
            .then(profile => {
                return profile;
            });
    }
}
