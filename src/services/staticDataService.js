import { HttpService } from '../services/httpService';
import { json } from 'aurelia-fetch-client';

export class StaticDataService {
    constructor() {
        this.httpClient = new HttpService().getClient();
    }

    getCountries() {
        return Promise.resolve(countries);
    }

    patch(changeObj) {
        return this.httpClient
            .fetch('/v1/profile', {
                method: 'PATCH',
                body: json(changeObj)
            })
            .then(response => response.json())
            .then(profile => {
                return profile;
            });
    }
}
