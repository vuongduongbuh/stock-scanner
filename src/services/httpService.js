import { HttpClient } from 'aurelia-fetch-client';
export class HttpService {
    constructor(httpClient) {
        this.httpClient = new HttpClient().configure(config => {
            let host = 'https://api.dev.eventually.io';
            if (localStorage.getItem('env') === 'local') {
                host = 'http://localhost:7387';
            }

            config.withBaseUrl(host + '/api');
            config.withDefaults({
                headers: {
                    'Accept-Language': localStorage.getItem('language'),
                    Authorization: 'Bearer ' + localStorage.getItem('authToken')
                }
            });
        });
    }

    getClient() {
        return this.httpClient;
    }
}
