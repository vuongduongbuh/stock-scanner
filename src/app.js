import { HttpService } from './services/httpService';

import 'bootstrap';

export class App {
    constructor() {
    }

    configureRouter(config, router) {
        this.router = router;
        config.title = 'SmartUP Contracts';
        config.map([
            {
                route: ['', 'home'],
                name: 'Home',
                moduleId: './components/home/home',
                nav: false,
                title: 'Home'
            }
        ]);
        config.mapUnknownRoutes('./components/home/home');
    }
}
