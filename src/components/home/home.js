import { inject } from 'aurelia-framework';
import { HttpClient } from 'aurelia-fetch-client';
import STOCKs from '../../assets/data/stocks.json';
import FULL_STOCKS from '../../assets/data/full_stocks.json';
import * as _ from 'lodash';
export class Home {
    constructor() {
        this.httpClient = new HttpClient().configure(config => {
            let host = 'https://cors-anywhere.herokuapp.com/https://www.fireant.vn/api/';

            config.withBaseUrl(host);
            config.withDefaults({
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'RequestVerificationToken': 'twjtdg-6wMv7X61zsNEhWvMoFfIu514TZlD7_CtYjMnl5rhugpkvBJYMKxUKU8GYczKNktzt4PFCMO9RT_qmMd6r8gY1:ZBAR7pu3vNUWiL1Pg4A8UaNM83r8LHD6V-FnLZRa21YuluLUvuGlZbq2RN01wYML0tOmOflTd6aImzxdImrnBxRKEZU1'
                }
            });
        });

        this.stockResults = [];
        // this.httpClient.fetch('Data/Markets/TradingStatistic')
        //     .then(res => res.json())
        //     .then(data => {
        //         console.log(data);
        //         const arr = [];
        //         _.each(data, (e) => {
        //             if(e.BuyCount1w > 400) {
        //                 arr.push(e.Symbol);
        //             }
        //         })
        //         console.log(JSON.stringify(arr));
        //     })
        // console.log(CODES);
    }

    run() {
        this.number = 1;
        _.each(STOCKs, (code) => {
            setTimeout(() => {
                this.checkStock(code);
            }, 300);
        })
    }

    checkStock(stockCode) {
        const today = new Date();
        const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

        Promise.all(
            [
                this.httpClient.fetch('Data/Companies/HistoricalQuotes?symbol=' + stockCode + '&startDate=2018-1-1&endDate=' + date, { method: 'GET' }).then(res => res.json()),
                this.httpClient.fetch('Data/Markets/Quotes?symbols=' + stockCode, { method: 'GET' }).then(res => res.json())
            ]
        ).then(data => {
            // fs.writeFile(stockCode + ".json", data[0], function(err) {
            //     if (err) {
            //         console.log(err);
            //     }
            // });

            const canBuyableStock = this.isBuyableStock(data[0], data[1][0]);
            console.log(this.number);
            this.number++;
            if (canBuyableStock) {
                this.stockResults.push(stockCode);
            }
            return data;
        })
    }

    isBuyableStock(data, current) {
        console.log(current);
        const volume20Days = _.meanBy(_.takeRight(data, 20), (val) => {
            return val.DealVolume;
        });

        if (0.8* volume20Days > current.TotalVolume || current.TotalActiveBuyVolume < current.TotalActiveSellVolume) {
            return false;
        }

        // Get current value
        const arrayMA10 = _.takeRight(data, 10);
        const arrayMA20 = _.takeRight(data, 20);
        const arrayMA50 = _.takeRight(data, 50);

        const EMA10 = this.calculateEMA10Days(arrayMA10);

        const MA20 = _.meanBy(arrayMA20, (val) => {
            return val.PriceClose;
        });

        const MA50 = _.meanBy(arrayMA50, (val) => {
            return val.PriceClose;
        });

        const maxPrice = _.maxBy(data, (val) => {
            return val.PriceClose;
        });

        if (EMA10 < MA50 || MA20 < MA50) {
            return false;
        }

        // Get day - 1 value
        // const arrayMA10_1 = _.take(_.takeRight(data, 11), 10);
        // const arrayMA20_1 = _.take(_.takeRight(data, 21), 20);
        // const arrayMA50_1 = _.take(_.takeRight(data, 51), 50);

        // const EMA10_1 = this.calculateEMA10Days(arrayMA10_1);

        // const MA20_1 = _.meanBy(arrayMA20_1, (val) => {
        //     return val.PriceClose;
        // });

        // const MA50_1 = _.meanBy(arrayMA50_1, (val) => {
        //     return val.PriceClose;
        // });

        // if (EMA10_1 > EMA10 || MA20_1 > MA20 || MA50_1 > MA50) {
        //     return false;
        // }

        // Get day - 3 value
        // const arrayMA10_3 = _.take(_.takeRight(data, 17), 10);
        // const arrayMA20_3 = _.take(_.takeRight(data, 27), 20);

        // const EMA10_3 = this.calculateEMA10Days(arrayMA10_3);

        // const MA20_3 = _.meanBy(arrayMA20_3, (val) => {
        //     return val.PriceClose;
        // });

        // if (MA20_3 < EMA10_3) {
        //     return false;
        // }

        // const yesterdayValue = _.takeRight(data);
        // const currentValue = current;

        return true;
    }

    calculateEMA10Days(arrayMA10) {
        const EMA_CONST = 2 / 11;
        let result = arrayMA10[0].PriceClose;
        _.each(arrayMA10, (val) => {
            result = result * (1 - EMA_CONST) + val.PriceClose * EMA_CONST;
        });

        return result;
    }
}

