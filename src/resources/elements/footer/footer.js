import { customElement } from 'aurelia-framework';

import { inject } from 'aurelia-framework';

import { BindingSignaler } from 'aurelia-templating-resources';
import { EventAggregator } from 'aurelia-event-aggregator';
import { Router } from 'aurelia-router';

@customElement('footer')
@inject(Router, EventAggregator, BindingSignaler)
export class Footer {
    constructor(router, eventAggregator, bindingSignaler) {
        this.router = router;
        this.ea = eventAggregator;
        this.signaler = bindingSignaler;
    }

    attached() {}
}
